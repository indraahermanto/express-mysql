'use strict';
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    name: {
      type: DataTypes.STRING,
      validate: {
        isAlpha: true,
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: {
          msg: "Please enter a valid email address"
        }
      },
    },
    password: DataTypes.STRING,
    isAdmin: {
      type: DataTypes.BOOLEAN
    }
  }, {});

  users.associate = function(models) {
    // associations can be defined here
  };

  users.generateAuthToken = async function() {
    const user = this
    const token = jwt.sign({_id: user._id}, process.env.JWT_KEY)
    
    await user.save()
    return token
  }

  return users;
};