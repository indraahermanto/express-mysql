'use strict';
module.exports = (sequelize, DataTypes) => {
  const tokens = sequelize.define('tokens', {
    token: DataTypes.STRING,
    status: DataTypes.BOOLEAN
  }, {});
  tokens.associate = function(models) {
    // associations can be defined here
  };
  return tokens;
};